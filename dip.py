import json

# Чтение файла
with open('combined_results.txt', 'r') as file:
    data = json.load(file)

# Парсинг информации об ошибках
errors = data["errors"]

# Вывод ошибок в читаемой форме
for error in errors:
    print(f"Код ошибки: {error['code']}")
    print(f"Уровень: {error['level']}")
    print(f"Сообщение: {error['message']}")
    print(f"Путь: {error['path']}")
    print(f"ID правила: {error['rule_id']}")
    print(f"Тип ошибки: {error['type']}")
    print("---------------------------------------")